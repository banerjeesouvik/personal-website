import React from 'react'

import Github from '../Images/Github'
import Gitlab from '../Images/Gitlab'
import LinkedIn from '../Images/LinkedIn'
import Facebook from '../Images/Facebook'

import './style.css'

const Contacts = [
    {
        title: 'Github',
        icon: Github,
        src: 'https://github.com/banerjeesouvik'
    },
    {
        title: 'Gitlab',
        icon: Gitlab,
        src: 'https://gitlab.com/banerjeesouvik'
    },
    {
        title: 'LinkedIn',
        icon: LinkedIn,
        src: 'https://linkedin.com/in/banerjee-souvik'
    },
    {
        title: 'Facebook',
        icon: Facebook,
        src: 'https://facebook.com/banerjeesouvik18'
    }
]

const ContactSection = () => (
    <section className='contacts-conatiner'>
        {
            Contacts.map((Contact) => <span key={Contact.title} className='contact-icon-container'>
                <a href={Contact.src} target='_blank' rel='noopener noreferrer'>
                    <Contact.icon title={Contact.title} height='50' width='50' />
                </a>
            </span>)
        }
    </section>
)

export default ContactSection