import React from 'react'
import Avatar from '../components/Avatar'
import Descreption from '../components/Description'
import ContactsSection from '../components/ContactsSection'

import AvatarImg from '../assests/Souvik_Banerjee.jpeg'

import '../styles/homepage.css'


function HomePage() {
    return (
    <div className='container'>
        <div className='content'>
        <Avatar
            src={AvatarImg}
            alt='Souvik Banerjee'
        />
        <Descreption>
            Hello, I am <strong>Souvik Banerjee</strong>. I am a Web Developer. I love to code and explore new technologies.
            Apart from coding, I love travelling and reading books.
        </Descreption>
        <ContactsSection />
        </div>
    </div>
)}

export default HomePage